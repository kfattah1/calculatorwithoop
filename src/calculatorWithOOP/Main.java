package calculatorWithOOP;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        Scanner input = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.println("Menu:");
            System.out.println("1. Penjumlahan");
            System.out.println("2. Pengurangan");
            System.out.println("3. Perkalian");
            System.out.println("4. Pembagian");
            System.out.println("0. Keluar");
            System.out.print("Masukkan pilihan Anda: ");
            choice = input.nextInt();
            if (choice == 0) {
                System.out.println("Terima kasih!");
                break;
            }
            int a;
            int b;
            switch (choice) {
                case 1 -> {
                    int[] addInput = getInput(input);
                    a = addInput[0];
                    b = addInput[1];
                    System.out.println("Penjumlahan: " + calculator.add(a, b));
                }
                case 2 -> {
                    int[] subtractInput = getInput(input);
                    a = subtractInput[0];
                    b = subtractInput[1];
                    System.out.println("Pengurangan: " + calculator.subtract(a, b));
                }
                case 3 -> {
                    int[] multiplyInput = getInput(input);
                    a = multiplyInput[0];
                    b = multiplyInput[1];
                    System.out.println("Perkalian: " + calculator.multiply(a, b));
                }
                case 4 -> {
                    int[] divideInput = getInput(input);
                    a = divideInput[0];
                    b = divideInput[1];
                    try {
                        System.out.println("Pembagian: " + calculator.divide(a, b));
                    } catch (IllegalArgumentException e) {
                        System.out.println("Error: " + e.getMessage());
                    }
                }
                default -> System.out.println("Pilihan tidak valid!");
            }
        }
    }

    public static int[] getInput(Scanner input) {
        System.out.print("Masukkan bilangan pertama: ");
        int a = input.nextInt();
        System.out.print("Masukkan bilangan kedua: ");
        int b = input.nextInt();
        return new int[]{a, b};
    }
}
